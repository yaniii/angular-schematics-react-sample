import { Rule, Tree, SchematicContext } from '@angular-devkit/schematics';

export const addResultMessage = (): Rule => {
    return (_tree: Tree, _context: SchematicContext) => {
        _context.logger.log('info', `Done 🔥  See you later ✌️`);
        return _tree;
    }
}