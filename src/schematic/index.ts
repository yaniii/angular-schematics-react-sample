import {
    Rule,
    chain,
    SchematicContext,
    Tree
} from '@angular-devkit/schematics';

import { addWelcomMessage } from './utility/messages/welcom';
import { addResultMessage } from './utility/messages/result';
import { addFilesToTree } from './utility/add-files';

import { schemaOptions } from './schema';


// You don't have to export the function as default. You can also have more than one rule factory
// per file.
export function reactComponentSample(_options: schemaOptions): Rule {
    return (_tree: Tree, _context: SchematicContext) => {
        // Chain multiple rules into a single rule.
        return chain([
            () => addWelcomMessage(),
            () => addFilesToTree(_options),
            () => addResultMessage()
        ]);
    };
}
