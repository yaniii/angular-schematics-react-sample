# ReactJS generation code With Angular Schematics

This sample showing, how do you can use Angular Schematics for ReactJS.

Do you have any questions or problems, please contact me: 
- Telegram/Twitter: @yani4218
- email: yani4218@gmail.com

### Getting Started With Schematics

To test locally, create a Schematics project or called yourself Schematics at React project, first install the Schematics CLI:

```bash
npm i -g @angular-devkit/schematics-cli
```

Also сheck the documentation with:

```bash
schematics --help
```

At React project, install @angular-devkit/core:

```bash
npm i @angular-devkit/core
```

If you want, so that the schematic options are put through the x-prompt property from shema.json, install angular-cli globally:

```bash
npm i -g @angular-cli
```

### Build Schematics

`npm run build:schematics` will run building youre schematic.

### Unit Testing

`npm run test` will run the unit tests, using Jasmine as a runner and test framework.

### Publishing

To publish, simply do:

```bash
npm run build:schematics
npm publish
```

### Execute Schematics

- Step 1: Copied react-component from dist to node_modules youre ReactJS project or install schematic from npm
- Step 2: `schematics <path-to-collection.json>:<schematic-name or schematic-aliases> <schematic-options> --dry-run=false` will run execute youre schematic.
Example how run executing schematic, you can look at package.json -> scripts -> dev:rc

Good luck, my friend!
From scout-ngx team with ❤️
